// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information

#include <cstddef>
#include <cstdint>

#include "simplev_cpp.h"

sv::VL<8> test_setvl(std::size_t v)
{
    return sv::setvl<8>(v);
}